# -*- coding: utf-8 -*-

from sqlalchemy import (
    Column, Integer, String, ForeignKey, DateTime, create_engine, Table
)
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class DBContext(object):
    def __enter__(self):
        engine = create_engine('sqlite:///events.db')
        session = sessionmaker()
        session.configure(bind=engine)
        Base.metadata.create_all(engine)
        self.get_session = session

        return self

    def __exit__(self, *args):
        pass


def get_or_create(session, model, **kwargs):
    '''
    Returns an model based on kwargs if available or creates one.

    session -- active session connected to the database.
    model -- DB Model for the object.
    kwargs -- Filter arguments to find the object on.
    '''
    obj = session.query(model).filter_by(**kwargs).first()

    if not obj:
        obj = model(**kwargs)
        session.add(obj)
        session.commit()

    return obj


event_tag_set = Table(
    'event__event_category', Base.metadata,
    Column('event_id', Integer, ForeignKey('event__event.id')),
    Column('tag_id', Integer, ForeignKey('event__tag_category.id'))
)


class Event(Base):
    __tablename__ = 'event__event'

    id = Column(Integer, primary_key=True)
    title = Column(String(255), nullable=False)
    url = Column(String(2047), nullable=False)
    description = Column(String(4095), nullable=True)

    media_link_set = relationship('MediaLink', back_populates='event')
    time_set = relationship('EventTime', back_populates='event')
    raw_info = relationship('RawEvent', back_populates='event', uselist=False)

    tag_set = relationship(
        'TagCategory', secondary=event_tag_set, back_populates='event_set'
    )


class RawEvent(Base):
    __tablename__ = 'event__raw_event'

    id = Column(Integer, primary_key=True)
    address = Column(String(255))
    cost = Column(String(255))
    times = Column(String(255))
    event_id = Column(Integer, ForeignKey('event__event.id'))

    event = relationship('Event', back_populates='raw_info')


class TagCategory(Base):
    __tablename__ = 'event__tag_category'

    id = Column(Integer, primary_key=True)
    tag_name = Column(String(31), nullable=False)

    event_set = relationship(
        'Event', secondary=event_tag_set, back_populates='tag_set'
    )


class EventTime(Base):
    __tablename__ = 'event__event_time'

    id = Column(Integer, primary_key=True)
    start = Column(DateTime)
    end = Column(DateTime)
    event_id = Column(Integer, ForeignKey('event__event.id'))

    event = relationship('Event', back_populates='time_set')


class MediaLink(Base):
    __tablename__ = 'event__media_links'

    id = Column(Integer, primary_key=True)
    site = Column(String(63), nullable=True)
    link = Column(String(255), nullable=False)
    event_id = Column(Integer, ForeignKey('event__event.id'))

    event = relationship('Event', back_populates='media_link_set')
