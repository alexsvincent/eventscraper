#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse
from datetime import datetime

from db_classes import (
    DBContext, get_or_create, Event, RawEvent, TagCategory,
    EventTime, MediaLink
)

from whatson_scraper import WhatsOnEventCrawler


def validate_date(date):
    ''' Validates and returns user input. '''

    try:
        return datetime.strptime(date, '%Y-%m-%d').strftime('%Y-%m-%d')
    except ValueError:
        msg = 'Date: {} must be a valid date of format YYYY-MM-DD'.format(date)
        raise argparse.ArgumentError(msg)


def run(start_date, end_date):
    ''' Runs through a set of pages and saves them to db classes. '''

    crawler = WhatsOnEventCrawler()
    with DBContext() as db:
        for page in crawler.crawl_pages(start_date, end_date):
            sess = db.get_session()

            categories = []
            for s in page['categories']:
                categories.append(get_or_create(sess, TagCategory, tag_name=s))

            media_links = []
            for ml in page['social_media']:
                media_links.append(MediaLink(site=ml['site'], link=ml['link']))

            event_times = []
            for ev_date in page['date_list']:
                event_times.append(
                    EventTime(
                        start=datetime.combine(ev_date, page['start_time']),
                        end=datetime.combine(ev_date, page['end_time'])
                    )
                )

            raw_ev = RawEvent(
                address=page['address_raw'], cost=page['cost_raw'],
                times=page['times_raw']
            )

            ev = Event(
                title=page['title'],
                url=page['url'],
                description=page['description'],
                media_link_set=media_links,
                raw_info=raw_ev,
                tag_set=categories,
                time_set=event_times
            )

            sess.add(ev)
            sess.commit()


if __name__ == '__main__':
    DESCRIPTION = '''
A script to scrape and store data from whatson.cityofsydney.nsw.gov.au
'''
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument(
        '-s', '--startdate', required=True, type=validate_date,
        help='Start date for events to scrape -- format YYYY-MM-DD'
    )
    parser.add_argument(
        '-e', '--enddate', required=True, type=validate_date,
        help='End date for events to scrape -- format YYYY-MM-DD'
    )

    args = parser.parse_args()
    run(args.startdate, args.enddate)
