# -*- coding: utf-8 -*-

import requests
import json
from bs4 import BeautifulSoup
from dateutil.parser import parse


class RequestException(Exception):
    pass


class WhatsOnEventCrawler(object):
    search_url = (
        'https://whatson.cityofsydney.nsw.gov.au/api/v1/search/advanced?' +
        'date%5B%5D={date_to}&date%5B%5D={date_from}&page={page_no}'
    )
    page_url = (
        'https://whatson.cityofsydney.nsw.gov.au/events/{page_slug}'
    )

    def crawl_pages(self, date_from, date_to):
        '''
        Loops over event information from the City Of Sydney Event API.

        date_from -- Crawl starts at this date
        date_to --  Crawl finishes at this date
        '''

        page_no = 1
        event_list = True

        while event_list:
            page_data = requests.get(self.search_url.format(
                date_to=date_to, date_from=date_from, page_no=page_no
            ))

            if page_data.status_code != 200:
                raise RequestException('Unable to get event listing page')

            event_list = json.loads(page_data.content).get('data', [])

            for event in event_list:
                page_scraper = WhatsOnPageScraper(
                    self.page_url.format(page_slug=event.get('slug')),
                    self.get_event_params(event)
                )
                yield page_scraper.run()

            page_no += 1

    @staticmethod
    def get_event_params(event):
        '''
        Returns event parameters extracted and cleaned from api json data.

        event -- dictionary of data related to an event.
        '''
        event_params = {
            'date_list': [],
            'start_time': None,
            'end_time': None,
            'categories': []
        }

        if 'details' in event and 'dates_list' in event['details']:
            event_params['date_list'] = [
                parse(i).date() for i in event['details']['dates_list']
            ]

        if 'details' in event and 'json_date' in event['details']:
            event_params['start_time'] = parse(
                event['details']['json_date'].get('startTime')
            ).time()

            event_params['end_time'] = parse(
                event['details']['json_date'].get('endTime')
            ).time()

        if 'tags' in event:
            event_params['categories'] = [
                i.get('slug') for i in event['tags']
                if i.get('slug') is not None
            ]

        return event_params


class WhatsOnPageScraper(object):
    def __init__(self, page, page_dict={}):
        self.url = page

        self.page_data = page_dict

        self.page_data.update({
            'url': page,
            'title': None,
            'description': None,
            'address_raw': None,
            'cost_raw': None,
            'times_raw': None,
            'social_media': []
        })

    def request_page(self):
        ''' Requests page, throws RequestException on error.'''

        page = requests.get(self.url)

        if page.status_code != 200:
            raise RequestException('Unable to get event page')

        return page

    def process_page(self, data):
        soup = BeautifulSoup(data.content, 'lxml')
        self.page_data['url'] = data.url
        self.page_data['title'] = self._get_clean_text(
            soup.find('h1', attrs={
                'class': 'page-content-title event-title'
            })
        )

        self.page_data['description'] = self._get_clean_text(
            soup.find('section', attrs={
                'class': 'event-single-description'
            })
        )

        self.page_data['address_raw'] = self._get_clean_text(
            soup.find('dt', attrs={
                'class': 'details-list-term details-list-term-where'
            })
            .find_next_sibling('dd')
        )

        self.page_data['cost_raw'] = self._get_clean_text(
            soup.find('dt', attrs={
                'class': 'details-list-term details-list-term-cost'
            })
            .find_next_sibling('dd')
        )

        self.page_data['times_raw'] = self._get_clean_text(
            soup.find('dt', attrs={
                'class': 'details-list-term details-list-term-when'
            })
            .find_next_sibling('dd')
        )

        media_links = (
            soup.find('dt', attrs={
                'class': 'details-list-term details-list-term-info'
            })
            .find_next_sibling('dd').find_all('a')
        )

        for link in media_links:
            if 'http' in link['href'] or 'www' in link['href']:
                self.page_data['social_media'].append({
                    'site': self._get_clean_text(link),
                    'link': link['href']
                })

    @staticmethod
    def _get_clean_text(element):
        return '\n'.join([s for s in element.stripped_strings])

    def get_page_data(self):
        return self.page_data

    def run(self):
        ''' Helper method that processes page and returns event data. '''

        page = self.request_page()
        self.process_page(page)

        return self.get_page_data()

if __name__ == '__main__':
    obj = WhatsOnEventCrawler()
    obj.crawl_pages('2017-03-19', '2017-03-19')
